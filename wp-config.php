<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projetos_royalclubpoker_site');

/** MySQL database username */
define('DB_USER', 'root');


/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?&sn+2Qcao M+N[9vhfO>Xf=@31$ck`y{?LDLt4r#O:Rc;hjQM<+/,?BFHm1fE+^');
define('SECURE_AUTH_KEY',  'n@d*$^Los/#/ .gMoz}y`j#yV*pV-G5Y(g!2d%Q#.Gr&dVn?xMj.lOm:D_W>wQ7F');
define('LOGGED_IN_KEY',    'L%%ZO`I<VDs!PXUfT{21r&N2a5:&jcY4Fg?k5zEUjH85IxK4?kIh:U2F{E}~`ev1');
define('NONCE_KEY',        'G3SC%xVR!QF{FG;OGX_U5|?s-W9^70GS{2$_/fG]j2O 6,9L7$S]XZtC}wNINl!-');
define('AUTH_SALT',        'R|60WDQj{e_5[9T(Ox9Q?eQ~g_xPfwniY`?P{iwbN0S0[ZvQMup/k[N/)#Z.#KFR');
define('SECURE_AUTH_SALT', 'A$)LqA|t&hSH8~yk}Ux%lr#Fbt!9~HluO;wit}jQW<8AUxEzKXs=,SpPSymw;=K/');
define('LOGGED_IN_SALT',   'MZ^^!0af#dymaclSjG/=M s-0%.;^Mi2fuO;`DX1)5}6m5yt{b#fg @r+-O$6w?.');
define('NONCE_SALT',       'SP?h-u`[;cBnH4/}!KUsKHxu8cd#=N0o[N0Rc0U[-+HDAp^JyhZ_!j8GC+Wmc<*i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
