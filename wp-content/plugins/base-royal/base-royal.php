<?php
/**
 * Plugin Name: Base Royal Club Poker
 * Description: Controle base do tema Royal Club Poker.
 * Version: 0.1
 * Author: Handgran
 * Author URI: 
 * Licence: GPL2
 */

	function baseRoyal () {

		//TIPOS DE CONTEÚDO
		conteudosRoyal();

		//TAXONOMIA
		taxonomiaRoyal();

		//META BOXES
		metaboxesRoyal();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
		function conteudosRoyal (){

			// TIPOS DE DESTQUE
			tipoDestaque();

			// TIPOS DE EVENTOS
			tipoEventos();


			/* ALTERAÇÃO DO TÍTULO PADRÃO */
			add_filter( 'enter_title_here', 'trocarTituloPadrao' );
			function trocarTituloPadrao($titulo){

				switch (get_current_screen()->post_type) {

					case 'equipe':
						$titulo = 'Nome do integrante';
					break;

					default:
					break;
				}
			    return $titulo;
			}

		}
	
	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE eventos
		function tipoEventos() {

			$rotulosEventos = array(
									'name'               => 'Evento',
									'singular_name'      => 'evento',
									'menu_name'          => 'Eventos',
									'name_admin_bar'     => 'Eventos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo evento',
									'new_item'           => 'Novo evento',
									'edit_item'          => 'Editar evento',
									'view_item'          => 'Ver evento',
									'all_items'          => 'Todos os evento',
									'search_items'       => 'Buscar evento',
									'parent_item_colon'  => 'Dos evento',
									'not_found'          => 'Nenhum evento cadastrado.',
									'not_found_in_trash' => 'Nenhum evento na lixeira.'
								);

			$argsEventos 	= array(
									'labels'             => $rotulosEventos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-calendar-alt',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'torneios' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('evento', $argsEventos);

		}


	/****************************************************
	* META BOXES
	*****************************************************/
		function metaboxesRoyal(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'Royal_';

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxTratamento',
					'title'			=> 'Detalhes do tratamento',
					'pages' 		=> array('tratamento'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Breve descrição: ',
							'id'    => "{$prefix}tratamento_breve_descricao",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Ícone do tratamento: ',
							'id'    => "{$prefix}tratamento_icone",
							'desc'  => '',
							'type'  => 'image_advanced',
							'max_file_uploads' => 1	
						), 
						array(
							'name'  => 'Galeria de fotos: ',
							'id'    => "{$prefix}tratamento_galeria_Fotos",
							'desc'  => '',
							'type'  => 'image_advanced',
						), 
						
					),
				);

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxEventos',
					'title'			=> 'Detalhes do evento',
					'pages' 		=> array('evento'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Galeria de fotos: ',
							'id'    => "{$prefix}evento_galeria",
							'desc'  => '',
							'type'  => 'image_advanced',
						), 
						array(
							'name'  => 'Data do evento: ',
							'id'    => "{$prefix}evento_data",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Premiação: ',
							'id'    => "{$prefix}evento_premio",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);


				return $metaboxes;
			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaRoyal () {
			 taxonomiaCategoriaEventos();
		}

		function taxonomiaCategoriaEventos() {

			$rotulosCategoriaEventos = array(
												'name'              => 'Categorias de evento',
												'singular_name'     => 'Categorias de eventos',
												'search_items'      => 'Buscar categoria do evento',
												'all_items'         => 'Todas as categorias',
												'parent_item'       => 'Categoria pai',
												'parent_item_colon' => 'Categoria pai:',
												'edit_item'         => 'Editar categoria do evento',
												'update_item'       => 'Atualizar categoria',
												'add_new_item'      => 'Nova categoria',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias eventos',
											);

			$argsCategoriaEventos 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaEventos,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-eventos' ),
											);

			register_taxonomy( 'categoriaeventos', array( 'evento' ), $argsCategoriaEventos);

		}
		
		function metaboxjs(){



			global $post;

			$template = get_post_meta($post->ID, '_wp_page_template', true);

			$template = explode('/', $template);

			$template = explode('.', $template[1]);

			$template = $template[0];



			if($template != ''){

				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);

			}

		}


  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'baseRoyal');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {

	    	baseRoyal();

	   		flush_rewrite_rules();
		}

		register_activation_hook( __FILE__, 'rewrite_flush' );