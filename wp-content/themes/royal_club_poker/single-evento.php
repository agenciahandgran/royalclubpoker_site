<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package royal_club_poker
 */
 $fotoEventos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoEventos = $fotoEventos[0];
get_header(); ?>

  <div class="section-evento-selecionado">
    <div class="div-evento-selecionado-tabs">
      <div class="w-row">
        <div class="w-col w-col-6">
          <h1 class="heading-evento-selecionado">EVENTO</h1>
          <h1 class="heading-17"><?php echo get_the_title() ?></h1>

          <p class="paragraph"><?php  while ( have_posts() ) : the_post(); echo the_content(); endwhile; ?></p>

        </div>
        <div class="w-col w-col-6"><img src="<?php echo $fotoEventos  ?>" class="imgEvento" srcset="<?php echo $fotoEventos  ?> 500w, <?php echo $fotoEventos  ?> 555w" sizes="(max-width: 578px) 96vw, (max-width: 767px) 555px, 48vw"></div>
      </div>
    </div>
    
    
    <div class="div-eventos-tab">
      <h1 class="heading-13">AGENDA DE EVENTOS</h1>
    </div>
    <div data-duration-in="300" data-duration-out="100" class="tabs-3 w-tabs">

      <div class="w-tab-menu">
        
        <?php 
          // RECUPERANDO CATEGORIAS
          $categoriaeventos = array(
            'taxonomy'     => 'categoriaeventos',
            'child_of'     => 0,
            'parent'       => 0,
            'orderby'      => 'name',
            'pad_counts'   => 0,
            'hierarchical' => 1,
            'title_li'     => '',
            'hide_empty'   => 0
          );
          $listaCategorias = get_categories($categoriaeventos);
          $listaCategorias2 = get_categories($categoriaeventos);
          
        $i = 0;
        foreach ($listaCategorias as $listaCategorias):
          $listaCategorias = $listaCategorias -> name;
         ?>
        <a data-w-tab="Tab <?php echo $i ?>" class="tab-link w-inline-block w-tab-link w--current">
          <div><?php echo $listaCategorias; ?></div>
        </a>
        <?php $i++; endforeach; ?>
      </div>

      <div class="w-tab-content">
        <?php 
          $j = 0;
          foreach ($listaCategorias2 as $listaCategorias2){
            $listaCategorias2 -> name;
         ?>
        <div data-w-tab="Tab <?php echo $j ?>" class="w-tab-pane w--tab-active">
          
          <div class="div-evento-1">
            <div class="div-line-tab"></div>
            
            <?php 
              $postEventos = new WP_Query(array(
                'post_type'     => 'evento',
                'posts_per_page'   => -1,
                'tax_query'     => array(
                  array(
                    'taxonomy' => 'categoriaeventos',
                    'field'    => 'slug',
                    'terms'    => $listaCategorias2,
                    )
                  )
                )
              );
              while ( $postEventos->have_posts() ) : $postEventos->the_post();
             ?> 
            <div class="row-6 w-row">
              <div class="w-col w-col-3">
                <h1 class="heading-14"><?php echo $telerradiologia_img =  rwmb_meta('Royal_evento_data'); ?></h1>
              </div>
              <div class="w-col w-col-3">
                <h1 class="heading-14"><?php echo get_the_title() ?></h1>
              </div>
              <div class="w-col w-col-3">
                <h1 class="heading-14"><?php echo $telerradiologia_img =  rwmb_meta('Royal_evento_premio'); ?></h1>
              </div>
              <div class="w-col w-col-3"><a href="<?php echo get_permalink() ?> " class="button-2 w-button">Saiba mais</a></div>
            </div>

            <div class="div-block-8"></div>
            <?php endwhile;wp_reset_query(); ?>
            
        
          </div>

        </div>
        <?php $j++;  }; ?>

      </div>
    </div>

    <div class="containerFull">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="slider-5 w-slider">
      <div class="w-slider-mask">
        <?php 
        $servico_breveDescricao = rwmb_meta('Royal_evento_galeria'); 
        foreach ($servico_breveDescricao as $servico_breveDescricao):
          $servico_breveDescricao = $servico_breveDescricao['full_url'];

          ?>
          <div class="w-slide">
            <img src="<?php echo $servico_breveDescricao ?>" srcset="<?php echo $servico_breveDescricao ?> 500w, <?php echo $servico_breveDescricao ?> 800w, images/<?php echo $servico_breveDescricao ?> 1080w, <?php echo $servico_breveDescricao ?> 1140w" sizes="(max-width: 1140px) 100vw, 1140px">
          </div>
        <?php endforeach; ?>


      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
    </div>

  </div>
   

   

<?php
get_footer();
