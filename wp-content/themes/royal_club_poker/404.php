<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package royal_club_poker
 */

get_header(); ?>

<div class="section-title-eventos">
		<div class="div-title-eventos">
			<h1 class="heading-11"><?php echo $configuracao['evento_titulo'] ?></h1>
			<p class="paragraph-2"><?php echo $configuracao['evento_texto'] ?></p>
			<h1 class="heading-12">PRÓXIMOS EVENTOS</h1>
		</div>
	</div>
	<div class="section-eventos">
		<div class="div-eventos">
		 <?php 
  		//LOOP DE POST DESTAQUES
	      $postEventos = new WP_Query( array( 'post_type' => 'evento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );
	      while ( $postEventos->have_posts() ) : $postEventos->the_post();
	        $fotoEventos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	        $fotoEventos = $fotoEventos[0];
	     ?>
			<div class="event-block" style="background: url(<?php echo $fotoEventos ?>)">
				<h1 style="visibility: hidden;font-size:0"><?php echo get_the_title() ?></h1>
				<div class="w-container">
					<a href="<?php echo get_permalink() ?>" class="button w-button">Saiba Mais</a>
				</div>
			</div>
			 <?php endwhile; wp_reset_query(); ?>
		</div>


		<div class="div-eventos-tab">
			<h1 class="heading-13">AGENDA DE EVENTOS</h1>
		</div>
		<div data-duration-in="300" data-duration-out="100" class="tabs-3 w-tabs">

			<div class="w-tab-menu">
				
				<?php 
					// RECUPERANDO CATEGORIAS
					$categoriaeventos = array(
						'taxonomy'     => 'categoriaeventos',
						'child_of'     => 0,
						'parent'       => 0,
						'orderby'      => 'name',
						'pad_counts'   => 0,
						'hierarchical' => 1,
						'title_li'     => '',
						'hide_empty'   => 0
					);
					$listaCategorias = get_categories($categoriaeventos);
					$listaCategorias2 = get_categories($categoriaeventos);
					
				$i = 0;
				foreach ($listaCategorias as $listaCategorias):
					$listaCategorias = $listaCategorias -> name;
				 ?>
				<a data-w-tab="Tab <?php echo $i ?>" class="tab-link w-inline-block w-tab-link w--current">
					<div><?php echo $listaCategorias; ?></div>
				</a>
				<?php $i++; endforeach; ?>
			</div>

			<div class="w-tab-content">
				<?php 
					$j = 0;
					foreach ($listaCategorias2 as $listaCategorias2){
						$listaCategorias2 -> name;
				 ?>
				<div data-w-tab="Tab <?php echo $j ?>" class="w-tab-pane w--tab-active">
					
					<div class="div-evento-1">
						<div class="div-line-tab"></div>
						
						<?php 
							$postEventos = new WP_Query(array(
								'post_type'     => 'evento',
								'posts_per_page'   => -1,
								'tax_query'     => array(
									array(
										'taxonomy' => 'categoriaeventos',
										'field'    => 'slug',
										'terms'    => $listaCategorias2,
										)
									)
								)
							);
							while ( $postEventos->have_posts() ) : $postEventos->the_post();
						 ?> 
						<div class="row-6 w-row">
							<div class="w-col w-col-3">
								<h1 class="heading-14"><?php echo $telerradiologia_img =  rwmb_meta('Royal_evento_data'); ?></h1>
							</div>
							<div class="w-col w-col-3">
								<h1 class="heading-14"><?php echo get_the_title() ?></h1>
							</div>
							<div class="w-col w-col-3">
								<h1 class="heading-14"><?php echo $telerradiologia_img =  rwmb_meta('Royal_evento_premio'); ?></h1>
							</div>
							<div class="w-col w-col-3"><a href="<?php echo get_permalink() ?> " class="button-2 w-button">Saiba mais</a></div>
						</div>

						<div class="div-block-8"></div>
						<?php endwhile;wp_reset_query(); ?>
						
				
					</div>

				</div>
				<?php $j++;  }; ?>

			</div>
		</div>
	</div>
	<div class="ultimo-evento">
		<div class="div-blog-ultimo-evento">
			<div class="row-2 w-row">
				<div class="column-2 w-col w-col-6">
					<h1 class="heading-18">ÚLTIMO EVENTO</h1>
					<?php 
						$postEventos = new WP_Query( array( 'post_type' => 'evento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 1) );
	     				while ( $postEventos->have_posts() ) : $postEventos->the_post();
					?>
					<h1 class="heading-19"><?php echo get_the_title() ?></h1>
					<p class="paragraph-3"><?php echo get_the_content() ?></p>
				<?php endwhile; wp_reset_query(); ?>
				</div>
				<div class="w-col w-col-6">
					
					<div data-animation="slide" data-duration="500" data-infinite="1" class="slider-3 w-slider">
						<?php 
							$postEventos = new WP_Query( array( 'post_type' => 'evento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 1) );
		     				while ( $postEventos->have_posts() ) : $postEventos->the_post();

						?>
						<div class="w-slider-mask">
							<?php 
								$servico_breveDescricao = rwmb_meta('Royal_evento_galeria'); 
								foreach ($servico_breveDescricao as $servico_breveDescricao):
									$servico_breveDescricao = $servico_breveDescricao['full_url'];
								
							?>
							<div class="slide-2 w-slide" style="background: url(<?php echo $servico_breveDescricao ?>);background-position: center!important;background-size: cover!important;"></div>
							<?php endforeach; ?>
						</div>
						<?php endwhile; wp_reset_query(); ?>
						<div class="w-slider-arrow-left">
							<div class="w-icon-slider-left"></div>
						</div>
						<div class="w-slider-arrow-right">
							<div class="w-icon-slider-right"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<?php
get_footer();
