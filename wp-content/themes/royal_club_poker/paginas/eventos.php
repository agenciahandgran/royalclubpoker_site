<?php  

/**
 * Template Name: Eventos
 * Description: Eventos
 *
 * @package Royal Club Poker
 */

  get_header();

?>

<div class="section-title-eventos">
  <div class="div-title-eventos">
    <h1 class="heading-11">Eventos Royal</h1>
    <p class="paragraph-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut libero luctus, varius turpis sed, accumsan justo. Nunc efficitur mauris vitae nibh aliquet, eget lobortis arcu pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
    <h1 class="heading-12">PRÓXIMOS EVENTOS</h1>
  </div>
</div>
<div class="section-eventos">
  <div class="div-eventos">
    <div class="event-block">
      <h1 class="heading-3">5 JUN</h1>
      <h1 class="heading-4">Royal Flush</h1>
      <h1 class="heading-5">5K</h1>
      <div class="w-container"><a href="pagina-de-evento.html" class="button w-button">Eventos</a></div>
    </div>
    <div class="event-block">
      <h1 class="heading-3">15 JUL</h1>
      <h1 class="heading-4">Straight Flush</h1>
      <h1 class="heading-5">5K</h1>
      <div class="w-container"><a href="#" class="button w-button">Eventos</a></div>
    </div>
    <div class="event-block">
      <h1 class="heading-3">20 JUL</h1>
      <h1 class="heading-4">Full House</h1>
      <h1 class="heading-5">5K</h1>
      <div class="w-container"><a href="#" class="button w-button">Eventos</a></div>
    </div>
  </div>
  <div class="div-eventos-tab">
    <h1 class="heading-13">AGENDA DE EVENTOS</h1>
  </div>
  <div data-duration-in="300" data-duration-out="100" class="tabs-3 w-tabs">
    <div class="w-tab-menu">
      <a data-w-tab="Tab 1" class="tab-link w-inline-block w-tab-link w--current">
        <div>Maio 2017</div>
      </a>
      <a data-w-tab="Tab 2" class="tab-link-5 w-inline-block w-tab-link">
        <div>Junho 2017</div>
      </a>
      <a data-w-tab="Tab 3" class="tab-link-5 w-inline-block w-tab-link">
        <div>Julho 2017</div>
      </a>
      <a data-w-tab="Tab 4" class="tab-link-5 w-inline-block w-tab-link">
        <div>Agosto 2017</div>
      </a>
    </div>
    <div class="w-tab-content">
      <div data-w-tab="Tab 1" class="w-tab-pane w--tab-active">
        <div class="div-evento-1">
          <div class="div-line-tab"></div>
          <div class="row-6 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="pagina-de-evento.html" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-7 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-8 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
        </div>
      </div>
      <div data-w-tab="Tab 2" class="w-tab-pane">
        <div class="div-evento-1">
          <div class="div-line-tab"></div>
          <div class="row-6 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-7 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-8 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
        </div>
      </div>
      <div data-w-tab="Tab 3" class="w-tab-pane">
        <div class="div-evento-1">
          <div class="div-line-tab"></div>
          <div class="row-6 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-7 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-8 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
        </div>
      </div>
      <div data-w-tab="Tab 4" class="w-tab-pane">
        <div class="div-evento-1">
          <div class="div-line-tab"></div>
          <div class="row-6 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-7 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
          <div class="row-8 w-row">
            <div class="w-col w-col-3">
              <h1 class="heading-14">5JUN - 19H</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">Royal Flush Poker Night</h1>
            </div>
            <div class="w-col w-col-3">
              <h1 class="heading-14">5K</h1>
            </div>
            <div class="w-col w-col-3"><a href="#" class="button-2 w-button">Saiba mais</a></div>
          </div>
          <div class="div-block-8"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="ultimo-evento">
  <div class="div-blog-ultimo-evento">
    <div class="row-2 w-row">
      <div class="column-2 w-col w-col-6">
        <h1 class="heading-18">ÚLTIMO EVENTO</h1>
        <h1 class="heading-19">ROYAL NIGHT</h1>
        <p class="paragraph-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.</p>
      </div>
      <div class="w-col w-col-6">
        <div data-animation="slide" data-duration="500" data-infinite="1" class="slider-3 w-slider">
          <div class="w-slider-mask">
            <div class="slide-2 w-slide"></div>
            <div class="w-slide"></div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php get_footer(); ?>