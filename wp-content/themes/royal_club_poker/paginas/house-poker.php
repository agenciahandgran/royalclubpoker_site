<?php  

/**
 * Template Name: HOUSE POKER
 * Description: HOUSE POKER
 *
 * @package Royal Club Poker
 */

?>
<?php global $configuracao; ?>
<!-- META -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo get_site_url()?>/wp-content/themes/royal_club_poker/css/bootstrap.min.css" />
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_site_url()?>/wp-content/themes/royal_club_poker/css/owl.carousel.css" />
<link rel="stylesheet" href="<?php echo get_site_url()?>/wp-content/themes/royal_club_poker/css/site.css" />

<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo get_site_url()?>/wp-content/themes/royal_club_poker/js/bootstrap.min.js"></script>
<script src="<?php echo get_site_url()?>/wp-content/themes/royal_club_poker/js/owl.carousel.min.js"></script>
<script src="<?php echo get_site_url()?>/wp-content/themes/royal_club_poker/js/geral.js"></script>




  <div class="pg pg-housePouker">
    <div class="titulo">
      <div class="row">
        <div class="col-xs-3">
          <div class="btnVoltar">
            <a href="<?php echo home_url('/')?>">
              <i class="fas fa-chevron-left"></i>
            </a>
          </div>
        </div>
        <div class="col-xs-9">
          <p><?php echo $configuracao['tittulo_house'] ?> <i class="fas fa-home"></i></p>
        </div>
      </div>
    </div>
    
    <div class="texto">
      <p><?php echo $configuracao['texto_house'] ?></p>
      <a href="<?php echo home_url('/contato/'); ?>" class="reserve">Reserve</a>
    </div>

    <div class="carrossel">
      <div class="button">
        <div class="row">
          <div class="col-xs-6">
            <button id="carrosselDestaqueLeft"><i class="fas fa-caret-left"></i></button>
          </div>
          <div class="col-xs-6">
            <button id="carrosselDestaqueRight"><i class="fas fa-caret-right"></i></button>
          </div>
        </div>
      </div>
      <div  id="carrosselDestaque" class="owl-Carousel">
        
        <?php 
          $galeria = $galeria = explode(',', $configuracao['opt_galeria_house']);
          foreach($galeria as $galeria):
        ?>
        <div class="item">
          <figure>
            <img src="<?php echo wp_get_attachment_url( $galeria ); ?>" alt="<?php echo $configuracao['tittulo_house'] ?>">
          </figure>
        </div>
         <?php endforeach; ?>
      </div>
    </div>


  </div>

  <div class="loading">
    <div class="info">
      <img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="">
      <div class="loader"></div>
    </div>
  </div>

   <footer>
    <div class="row">
      <div class="col-xs-6">
        <div class="redesSociais">
          <a href="<?php echo $configuracao['face'] ?>" target="_blank">
            <i class="fab fa-facebook-square"></i>
          </a>
          <a href="<?php echo $configuracao['instagram'] ?>" target="_blank">
            <i class="fab fa-instagram"></i>
          </a>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="desenvolvido">
          <a href="http://www.handgran.com" target="_blank">Desenvolvido por <img src="http://royalclubpoker.handgran.com.br/wp-content/themes/royal_club_poker/images/handgran-logo.png" alt=""></a>
        </div>
      </div>
    </div>
  </footer>