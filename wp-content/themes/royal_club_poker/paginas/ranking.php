<?php  

/**
 * Template Name: Ranking
 * Description: Ranking
 *
 * @package Royal Club Poker
 */
	
 get_header();
?>

<div class="section-title-eventos">
	<div class="div-title-eventos">
		<h1 class="heading-11">Royal Ranking</h1>
		<p class="paragraph-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut libero luctus, varius turpis sed, accumsan justo. Nunc efficitur mauris vitae nibh aliquet, eget lobortis arcu pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
		<h1 class="heading-12">LOREM IPSUM</h1>
	</div>
</div>
<div class="section-ranking-list">
	<div class="div-ranking-list">
		<div class="row-9 w-row">
			<div class="w-col w-col-3">
				<h1 class="heading-15">JOGADOR</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-15">ETAPA</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-15">COLOCAÇÃO</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-15">PONTOS</h1>
			</div>
		</div>
		<div class="div-line"></div>
		<div class="row-10 w-row">
			<div class="w-col w-col-3">
				<h1 class="heading-16">Roberto</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">Royal Flush Poker Night</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">1º</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">300</h1>
			</div>
		</div>
		<div class="div-line"></div>
		<div class="row-11 w-row">
			<div class="w-col w-col-3">
				<h1 class="heading-16">Maurício</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">Royal Flush Poker Night</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">2º</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">220</h1>
			</div>
		</div>
		<div class="div-line"></div>
		<div class="row-12 w-row">
			<div class="w-col w-col-3">
				<h1 class="heading-16">Ana Paula</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">Royal Flush Poker Night</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">3º</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">100</h1>
			</div>
		</div>
		<div class="div-line"></div>
		<div class="row-13 w-row">
			<div class="w-col w-col-3">
				<h1 class="heading-16">Jandrei</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">Royal Flush Poker Night</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">3º</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">80</h1>
			</div>
		</div>
		<div class="div-line"></div>
		<div class="row-14 w-row">
			<div class="w-col w-col-3">
				<h1 class="heading-16">Bruno</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">Royal Flush Poker Night</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">4º</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">40</h1>
			</div>
		</div>
		<div class="div-line"></div>
		<div class="row-15 w-row">
			<div class="w-col w-col-3">
				<h1 class="heading-16">Carlos</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">Royal Flush Poker Night</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">5º</h1>
			</div>
			<div class="w-col w-col-3">
				<h1 class="heading-16">20</h1>
			</div>
		</div>
		<div class="div-line"></div>
	</div>
</div>
<?php get_footer() ?>