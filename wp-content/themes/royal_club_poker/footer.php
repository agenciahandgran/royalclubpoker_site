<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package royal_club_poker
 */
global $configuracao;
$contato = explode(";",$configuracao['telefone']);
?>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3600.1218588987076!2d-49.21154978490009!3d-25.534317483740498!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dcfa0edde35e03%3A0x58e9cf1cf48833d3!2sAv.+Sen.+Souza+Naves%2C+863+-+Tres+Marias%2C+S%C3%A3o+Jos%C3%A9+dos+Pinhais+-+PR!5e0!3m2!1spt-BR!2sbr!4v1516800472074" width="100%" height="300" frameborder="0" style="border:0" ></iframe>

<div class="div_footer">
	<div class="div-block-3">
	  <div class="row-4 w-row">
	    <div class="w-col w-col-2"><img src="<?php echo $configuracao['opt_logoRodape']['url'] ?>"></div>
	    <div class="w-col w-col-3">
	      <h1 class="heading-6"><?php echo $configuracao['infoRodape'] ?></h1>
	    </div>
	    <div class="w-col w-col-2">
	      <h1 class="heading-7">CONTATO</h1>
	      <p class="paragraph-4"><?php echo $contato[0]; ?></p>
	      <p class="paragraph-4"><?php echo $contato[1]; ?></p>
	    </div>
	    <div class="w-col w-col-3">
	      <h1 class="heading-7">HORÁRIOS</h1>
	      <p class="paragraph-4"><?php echo $configuracao['horario'] ?></p>
	    </div>
	    <div class="w-col w-col-2">
	      <div class="row-5 w-row">
	      	<div class="column-3 w-col w-col-6">
	      		<a href="<?php echo $configuracao['face'] ?>" target="_blank">
	      			<img src="<?php echo get_template_directory_uri() ?>/images/facebook-icon-.png">
	      		</a>
	      	</div>
	        <div class="column-4 w-col w-col-6">
	        	<a href="<?php echo $configuracao['instagram'] ?>" target="_blank">
	        		<img src="<?php echo get_template_directory_uri() ?>/images/instagram-icon.png">
	        	</a>
	        </div>
	      </div>
	    </div>
	  </div><img src="<?php echo get_template_directory_uri()  ?>/images/line.png" srcset="<?php echo get_template_directory_uri() ?>/images/line-p-1600.png 1600w, <?php echo get_template_directory_uri()  ?>/images/line.png 1920w" sizes="(max-width: 991px) 100vw, 1170px" class="image-3">
	  <div class="w-row">
	    <div class="w-col w-col-9">
	      <h1 class="heading-7">© <?php echo $configuracao['opt_Copyryght'] ?></h1>
	    </div>
	    <div class="w-col w-col-2">
	      <h1 class="heading-8">Desenvolvido por</h1>
	    </div>
	    <div class="column-5 w-col w-col-1"><img src="<?php echo get_template_directory_uri() ?>/images/handgran-logo.png" class="image-2"></div>
	  </div>
	</div>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/webflow.js" type="text/javascript"></script>
<?php wp_footer(); ?>

</body>
</html>
