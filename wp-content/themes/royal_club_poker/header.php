<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package royal_club_poker
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
	<link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php if (get_the_title() == "Inicio"): ?>
	<div class="hero">
		<div data-animation="slide" data-disable-swipe="1" data-duration="500" data-infinite="1" class="slider w-slider">
			<div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
				<div class="container-2 w-container">
					<a href="<?php echo home_url('/'); ?>" class="brand w-nav-brand"></a>
					<nav role="navigation" class="nav-menu w-nav-menu">
						<a href="<?php echo home_url('/sobre/'); ?>" class="nav-link w-nav-link">Sobre</a>
						<a href="<?php echo home_url('/torneios/'); ?>" class="nav-link w-nav-link">Torneios</a>
						<!-- <a href="ranki.html" class="nav-link w-nav-link">Ranking</a> -->
						<a href="<?php echo home_url('/contato/'); ?>" class="nav-link w-nav-link">Contato</a>
					</nav>
					<div class="w-nav-button">
						<div class="w-icon-nav-menu"></div>
					</div>
				</div>
			</div>
			<div class="mask w-slider-mask">
				<?php 
					//LOOP DE POST DESTAQUES
					$posDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
					while ( $posDestaques->have_posts() ) : $posDestaques->the_post();
						$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestaque = $fotoDestaque[0];
				 ?>
				<div class="slide-1 w-slide" style="background: url(<?php echo $fotoDestaque ?>)">
					<div class="w-container">
						<h1 class="heading"><?php echo the_content() ?></h1>
					</div>
				</div>
				<?php endwhile; wp_reset_query(); ?>
				
			</div>
			<div class="left-arrow w-slider-arrow-left">
				<div class="icon w-icon-slider-left"></div>
			</div>
			<div class="w-slider-arrow-right">
				<div class="icon-2 w-icon-slider-right"></div>
			</div>
		</div>
	</div>
	<?php elseif (get_the_title() == "Sobre"): ?>

	<?php ?>
		<div class="hero-header-sobre" style="background: url(<?php echo $configuracao['sobre_foto']['url'] ?>)">
			<div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
				<div class="container-2 w-container"><a href="<?php echo home_url('/'); ?>" class="brand w-nav-brand"></a>
					<nav role="navigation" class="nav-menu w-nav-menu">
						<a href="<?php echo home_url('/sobre/'); ?>" class="nav-link w-nav-link">Sobre</a>
						<a href="<?php echo home_url('/torneios/'); ?>" class="nav-link w-nav-link">Torneios</a>
						<!-- <a href="ranki.html" class="nav-link w-nav-link">Ranking</a> -->
						<a href="<?php echo home_url('/contato/'); ?>" class="nav-link w-nav-link">Contato</a></nav>
					<div class="w-nav-button">
						<div class="w-icon-nav-menu"></div>
					</div>
				</div>
			</div>
			<div class="div-sobre-header" >
				<h1 class="heading-9"><?php echo $configuracao['sobre_titulo'] ?></h1>
				<p class="paragraph-8"><?php echo $configuracao['sobre_texto'] ?></p>
			</div>
		</div>
	<?php else: ?>
	<div class="hero-header-eventos">
		<div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
			<div class="container-2 w-container">
				<a href="<?php echo home_url('/'); ?>" class="brand w-nav-brand"></a>
				<nav role="navigation" class="nav-menu w-nav-menu">
					<a href="<?php echo home_url('/sobre/'); ?>" class="nav-link w-nav-link">Sobre</a>
					<a href="<?php echo home_url('/torneios/'); ?>" class="nav-link w-nav-link">Torneios</a>
					<!-- <a href="ranki.html" class="nav-link w-nav-link">Ranking</a> -->
					<a href="<?php echo home_url('/contato/'); ?>" class="nav-link w-nav-link">Contato</a>
				</nav>
				<div class="w-nav-button">
					<div class="w-icon-nav-menu"></div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>