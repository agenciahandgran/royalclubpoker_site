<?php global $configuracao; ?>
<!-- META -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" />
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/site.css" />

<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/geral.js"></script>

<div class="mobile">
	<div class="pg pg-inicial">

		<div class="row">

			<div class="col-xs-6">
				<a href="<?php echo home_url('/torneios/'); ?>" class="item-quadro" id="servico">
					<div class="caixa-centro">
						<i class="fas fa-trophy"></i>
						<h4>Torneios</h4>
					</div>
				</a>
			</div>

			<div class="col-xs-6">
				<a href="<?php echo home_url('/house-poker/')?>" class="item-quadro" id="orcamento">
					<div class="caixa-centro">
						<i class="fas fa-home"></i>
						<h4>HOUSE POKER</h4>
					</div>
				</a>
			</div>

			<div class="col-xs-6">
				<a href="<?php echo home_url('/sobre/')?>" class="item-quadro" id="ligar">
					<div class="caixa-centro">
						<i class="fas fa-info"></i>
						<h4>Sobre</h4>
					</div>
				</a>
			</div>

			<div class="col-xs-6">
				<a href="<?php echo home_url('/contato/')?>" class="item-quadro" id="contato">
					<div class="caixa-centro">
						<i class="fas fa-phone"></i>
						<h4>Contato</h4>
					</div>
				</a>
			</div>

		</div>

	</div>
</div>

<div class="loading">
	<div class="info">
		<img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="">
		<div class="loader"></div>
	</div>
</div>