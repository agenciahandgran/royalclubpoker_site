<?php global $configuracao; ?>
<!-- META -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" />
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/site.css" />

<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/geral.js"></script>

  <div class="pg pg-contato">
    <div class="titulo">
      <div class="row">
        <div class="col-xs-3">
          <div class="btnVoltar">
            <a href="<?php echo home_url('/')?>">
              <i class="fas fa-chevron-left"></i>
            </a>
          </div>
        </div>
        <div class="col-xs-9">
          <p>Contato <i class="fas fa-phone"></i></p>
        </div>
      </div>
    </div>

    <div class="formulario">
      <p><?php echo $configuracao['contato_numero'] ?><br><?php echo $configuracao['contato_endereco'] ?></p>

      <div class="form">
        <?php echo do_shortcode('[contact-form-7 id="42" title="Formulário de contato"]'); ?>
      </div>

      <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3600.1218588987076!2d-49.21154978490009!3d-25.534317483740498!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dcfa0edde35e03%3A0x58e9cf1cf48833d3!2sAv.+Sen.+Souza+Naves%2C+863+-+Tres+Marias%2C+S%C3%A3o+Jos%C3%A9+dos+Pinhais+-+PR!5e0!3m2!1spt-BR!2sbr!4v1516800472074" width="100%" height="300" frameborder="0" style="border:0"></iframe>
      </div>
      
    </div>
  </div>

  <div class="loading">
    <div class="info">
      <img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="">
      <div class="loader"></div>
    </div>
  </div>
<style> 
.screen-reader-response{
  display: none;
}
.wpcf7-response-output.wpcf7-validation-errors{
  border: solid 1px red;
  color: #fff;
  text-align: center;
  padding: 10px;
}
</style>
  <footer>
    <div class="row">
      <div class="col-xs-6">
        <div class="redesSociais">
          <a href="<?php echo $configuracao['face'] ?>" target="_blank">
            <i class="fab fa-facebook-square"></i>
          </a>
          <a href="<?php echo $configuracao['instagram'] ?>" target="_blank">
            <i class="fab fa-instagram"></i>
          </a>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="desenvolvido">
          <a href="http://www.handgran.com" target="_blank">Desenvolvido por <img src="http://royalclubpoker.handgran.com.br/wp-content/themes/royal_club_poker/images/handgran-logo.png" alt=""></a>
        </div>
      </div>
    </div>
  </footer>