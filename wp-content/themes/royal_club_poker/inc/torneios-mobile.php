<?php global $configuracao; ?>
<!-- META -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" />
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/site.css" />

<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/geral.js"></script>

<div class="pg pg-torneios">
    <div class="titulo">
      <div class="row">
        <div class="col-xs-3">
          <div class="btnVoltar">
            <a href="<?php echo home_url('/'); ?>">
              <i class="fas fa-chevron-left"></i>
            </a>
          </div>
        </div>
        <div class="col-xs-9">
          <p>Torneios <i class="fas fa-trophy"></i></p>
        </div>
      </div>
      
    </div>

    <div class="tituloTorneio">
      <h6><?php echo $configuracao['evento_titulo'] ?></h6>
      <p><?php echo $configuracao['evento_texto'] ?></p>
    </div>  
    <div class="torneitos">
      <div class="panel-group" id="accordion">
        <?php 
        $i = 0;
        //LOOP DE POST DESTAQUES
        $postEventos = new WP_Query( array( 'post_type' => 'evento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
        while ( $postEventos->have_posts() ) : $postEventos->the_post();
          $fotoEventos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
          $fotoEventos = $fotoEventos[0];
        ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i?>">
              <?php echo get_the_title() ?></a>
            </h4>
          </div>
          <div id="collapse<?php echo $i?>" class="panel-collapse collapse">
            <div class="panel-body">
              <img src="<?php echo $fotoEventos ?>" alt="<?php echo get_the_title() ?>">
              
              <button class="modalRegras">Regras <i class="fas fa-list-ol"></i></button>
              
              <div style="display: none;" data-title="<?php echo get_the_title() ?>">
                <?php  echo the_content() ?>
              </div>
           
            </div>
          </div>
        </div>
        <?php $i++;endwhile; wp_reset_query(); ?>

      </div>
    </div>
  </div>


<div class="loading">
    <div class="info">
      <img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="">
      <div class="loader"></div>
    </div>
  </div>

  <div id="modalRegras">
    <div class="regras">
      <strong id="titulo"></strong>
        <div id="texto"></div>
    </div>
  </div>

  <footer>
    <div class="row">
      <div class="col-xs-6">
        <div class="redesSociais">
          <a href="<?php echo $configuracao['face'] ?>" target="_blank">
            <i class="fab fa-facebook-square"></i>
          </a>
          <a href="<?php echo $configuracao['instagram'] ?>" target="_blank">
            <i class="fab fa-instagram"></i>
          </a>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="desenvolvido">
          <a href="http://www.handgran.com" target="_blank">Desenvolvido por <img src="http://royalclubpoker.handgran.com.br/wp-content/themes/royal_club_poker/images/handgran-logo.png" alt=""></a>
        </div>
      </div>
    </div>
  </footer>
