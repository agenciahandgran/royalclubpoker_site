<?php
 get_header();
?>
<div class="section-feature-sobre">
    <div class="div-features-sobre">
      
      <div class="w-row">
        <div class="column-6 w-col w-col-6">
          <img src="<?php echo $configuracao['sobre_icone_imagem1']['url'] ?>" class="image-6" style="max-width: 70px">
          <h1 class="heading-10"><?php echo $configuracao['opt_icone_titulo'] ?></h1>
          <p class="paragraph-9"><?php echo $configuracao['opt_icone_descricao'] ?></p>
        </div>
       <!--  <div class="column-7 w-col w-col-4">
          <img src="<?php echo $configuracao['sobre_icone_imagem2']['url'] ?>" class="image-5" style="max-width: 70px">
          <h1 class="heading-10"><?php echo $configuracao['opt_icone_titulo1'] ?></h1>
          <p class="paragraph-9"><?php echo $configuracao['opt_icone_descricao1'] ?></p>
        </div> -->
        <div class="column-8 w-col w-col-6">
          <img src="<?php echo $configuracao['sobre_icone_imagem3']['url'] ?>" style="max-width: 70px">
          <h1 class="heading-10"><?php echo $configuracao['opt_icone_titulo2'] ?></h1>
          <p class="paragraph-9"><?php echo $configuracao['opt_icone_descricao2'] ?></p>
        </div>
      </div>
      
      <div data-animation="slide" data-duration="500" data-infinite="1" class="slider-5 w-slider">
      
        <div class="w-slider-mask">
          <?php 
            $galeria = $galeria = explode(',', $configuracao['sobre_galeria']);
            foreach($galeria as $galeria):
          ?>
          <div class="w-slide">
            <img src="<?php echo wp_get_attachment_url( $galeria ); ?>" srcset="<?php echo wp_get_attachment_url( $galeria ); ?>, <?php echo wp_get_attachment_url( $galeria ); ?>, <?php echo wp_get_attachment_url( $galeria ); ?>, <?php echo wp_get_attachment_url( $galeria ); ?> 1140w" sizes="(max-width: 1140px) 100vw, 1140px">
          </div>
          <?php endforeach; ?>
        </div>
       
        <div class="w-slider-arrow-left">
          <div class="w-icon-slider-left"></div>
        </div>
       
        <div class="w-slider-arrow-right">
          <div class="w-icon-slider-right"></div>
        </div>
        <div class="w-slider-nav w-round"></div>
     
      </div>
    </div>
  </div>
  <div class="correcao" style="margin-bottom: 349px;"></div>

<?php get_footer(); ?>