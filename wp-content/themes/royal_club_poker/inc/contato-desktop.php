 <?php
 get_header();
?>

<div class="section-title-contato">
  <div class="div-title-eventos">
    <h1 class="heading-11">Contato</h1>
      <p class="paragraph-contato"><?php echo $configuracao['contato_numero'] ?><br><?php echo $configuracao['contato_endereco'] ?></p>
  </div>
</div>
<div class="section-form">
  <div class="div-form">
    <div class="w-form">
      
        <?php echo do_shortcode('[contact-form-7 id="42" title="Formulário de contato"]'); ?>

      <div class="w-form-done">
        <div>Thank you! Your submission has been received!</div>
      </div>
      <div class="w-form-fail">
        <div>Oops! Something went wrong while submitting the form.</div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>

